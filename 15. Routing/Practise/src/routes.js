import Home from './components/Home.vue'
import Header from './components/Header.vue'

// promises to components
const User = resolve => {
    require.ensure(['./components/user/User.vue'], () => {
        resolve(require('./components/user/User.vue'))
    }, 'user')
}
const UserStart = resolve => {
    require.ensure(['./components/user/UserStart.vue'], () => {
        resolve(require('./components/user/UserStart.vue'))
    })
}
const UserEdit = resolve => {
    require.ensure(['./components/user/UserEdit.vue'], () => {
        resolve(require('./components/user/UserEdit.vue'))
    })
}
const UserDetail = resolve => {
    require.ensure(['./components/user/UserDetail.vue'], () => {
        resolve(require('./components/user/UserDetail.vue'))
    }, 'user')
}

export const routes = [
    {
        path: '',
        name: 'home',
        components: {
            default: Home,
            'header-top': Header,
        },
    },
    {
        path: '/user',
        name: 'user',
        components: {
            default: User,
            'header-bottom': Header,
        },
        props: true,
        children: [
            {
                path: '',
                component: UserStart,
                props: true
            },
            {
                path: ':id',
                component: UserDetail,
                props: true,
                beforeEnter: (to, from, next) => {
                    console.log('UserDetail beforeEnter')
                    next()
                }
            },
            {
                path: ':id/edit',
                component: UserEdit,
                props: true,
                name: 'editRoute'
            },
        ]
    },
    {
        path: '/redirect-me',
        // redirect: '/user',
        redirect: {name: 'user'}
    },
    {
        path: '*',
        // redirect: '/',
        redirect: { name: 'home'}
    }
]