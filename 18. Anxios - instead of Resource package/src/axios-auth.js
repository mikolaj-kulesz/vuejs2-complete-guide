import axios from 'axios';

const axiosAuthInstance = axios.create({
    baseURL: 'https://identitytoolkit.googleapis.com/v1',
});

axiosAuthInstance.defaults.headers.common['Authorization'] = 'test1234';
axiosAuthInstance.defaults.headers.get['Accepts'] = 'application/json';

export default axiosAuthInstance;
