import Vue from 'vue';
import Vuex from 'vuex';
import axiosAuthInstance from './axios-auth';
import axios from 'axios';
import router from './router';

Vue.use(Vuex);

const getExpDate = (expTime) => {
    const now = new Date();
    return new Date(now.getTime() + (expTime * 1000));
};

export default new Vuex.Store({
    state: {
        tokenId: null,
        userId: null,
        users: [],
    },
    mutations: {
        authUser(state, {idToken, localId}) {
            state.tokenId = idToken;
            state.userId = localId;
        },
        storeUsers(state, userList) {
            state.users = userList;
        },
        logout(state) {
            state.tokenId = null;
            state.userId = null;
            state.users = [];
        },
    },
    actions: {
        singIn({commit, dispatch}, authData) {
            axiosAuthInstance.post('/accounts:signInWithPassword?key=AIzaSyBuZSyMjab1xx4MKANbmWS_ZKk7p0XD7JQ', {
                email: authData.email,
                password: authData.password,
                returnSecureToken: true,
            }).then(({data}) => {
                const {idToken, localId, expiresIn} = data;
                commit('authUser', {idToken, localId});
                dispatch('setAutoLogout', expiresIn);
                localStorage.setItem('idToken', idToken);
                localStorage.setItem('localId', localId);
                localStorage.setItem('expDate', getExpDate(expiresIn));
            }).catch(err => err);
        },
        singUp({commit, dispatch}, authData) {
            const {email, password} = authData;
            axiosAuthInstance.post('/accounts:signUp?key=AIzaSyBuZSyMjab1xx4MKANbmWS_ZKk7p0XD7JQ', {
                email,
                password,
                returnSecureToken: true,
            }).then(({data}) => {
                const {idToken, localId, expiresIn} = data;
                commit('authUser', {idToken, localId});
                dispatch('storeUser', authData);
                dispatch('setAutoLogout', expiresIn);
                localStorage.setItem('idToken', idToken);
                localStorage.setItem('localId', localId);
                localStorage.setItem('expDate', getExpDate(expiresIn));
            }).catch(err => err);
        },
        storeUser({commit, state}, authData) {
            axios.post('/user.json?auth=' + state.tokenId, authData).then(res => {
                // console.log('storeUser', res)
            }).catch(err => console.log(err));
        },
        fetchUsers({commit, state}, authData) {
            if (!state.tokenId) return;
            axios.get('/user.json?auth=' + state.tokenId).then(res => {
                const userList = Object.keys(res.data).map(key => {
                    return {
                        email: res.data[key].email,
                        id: key,
                    };
                });
                commit('storeUsers', userList);
            }).catch(err => err);
        },
        logout({commit}) {
            commit('logout');
            localStorage.removeItem('idToken');
            localStorage.removeItem('localId');
            localStorage.removeItem('expDate');
            router.replace('/signin');
        },
        setAutoLogout({commit}, exceptionTime) {
            setTimeout(() => {
                commit('logout');
            }, exceptionTime * 1000);
        },
        tryAutoLogin({commit}) {
            const idToken = localStorage.getItem('idToken');
            const localId = localStorage.getItem('localId');
            const expDate = localStorage.getItem('expDate');
            if(!idToken) return
            if(!localId) return
            if(!expDate) return
            if(new Date() >= expDate) return
            commit('authUser', {idToken, localId});
        },
    },
    getters: {
        users(state) {
            return state.users;
        },
        isAuthenticated(state) {
            return !!state.tokenId;
        },
    },
});
