import Vue from 'vue';
import App from './App.vue';

import router from './router';
import store from './store';
import axios from 'axios';
import Vuelidate from 'vuelidate'

Vue.use(Vuelidate)

axios.defaults.baseURL = 'https://vue-axios-a1256.firebaseio.com';
axios.defaults.headers.common['Authorization'] = 'test1234';
axios.defaults.headers.get['Accepts'] = 'application/json';

const reqInt = axios.interceptors.request.use(req => {
    console.log('req', req);
    return req;
});

const resInt = axios.interceptors.response.use(res => {
    console.log('res', res);
    return res;
});

// REMOVE interceptors
axios.interceptors.request.eject(reqInt)
axios.interceptors.response.eject(resInt)

new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App),
});
