import Vue from 'vue'
import App from './app.vue'
import DataBus from './data-bus.vue'

export const dataBus = new Vue(DataBus);

new Vue({
  el: '#app',
  render: h => h(App)
});
