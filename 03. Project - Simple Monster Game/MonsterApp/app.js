new Vue({
    el: '#app',
    data: {
        players: {
            human: {
                name: 'Human',
                health: 100,
                color: 'red'
            },
            monster: {
                name: 'Monster',
                health: 100,
                color: 'gray'
            },
        },
        gameStatus: false,
        logs: []
    },
    methods: {
        startGame: function() {
            this.gameStatus = true;
            this.logs = []
            Object
                .keys(this.players)
                .forEach((key) => {
                    this.players[key].health = 100;
                })
        },
        endGame: function() {
            this.gameStatus = false;
        },
        attack: function(max = 10, players = this.players) {
            Object
                .keys(players)
                .forEach((key) => {
                    this.singleAction(key, 'attack', max)
            })
        },
        singleAction: function(key, typeOfAction = 'attack', max = 10){
            let points = this.getRandom(1,max),
                action = typeOfAction === 'attack' ? 'minus' : 'plus',
                color = typeOfAction === 'attack' ? this.players[key].color : 'green',
                sum = this.players[key].health - points

            this.players[key].health = this.calculateInBoundaries(this.players[key].health, points, typeOfAction);
            this.createLog(this.players[key].name, points, color, action)
        },
        specialAttack: function() {
            this.attack(30);
        },
        heal: function(max = 10) {
            this.singleAction('human', 'heal')
            this.singleAction('human', 'attack')
        },
        giveUp: function() {
            this.players.human.health = 0
        },
        getRandom: function(min,max) {
            return Math.floor(Math.random() * max) + min;
        },
        createLog: function(who, number, color, action){
            this.logs.unshift({who, number, color, action})
        },
        displayConfirm: function(){
            return confirm(`The winner in ${this.winner.who} with ${this.winner.score} left. Do you want to start a new game?`)
        },
        showResults: function() {
            if (this.displayConfirm()) this.startGame()
            else this.endGame()
        },
        calculateInBoundaries: function(initialPoints, actionPoints, typeOfAction) {
            if(typeOfAction === 'attack') return Math.min(Math.max(0, initialPoints - actionPoints),100)
            else return Math.min(Math.max(0, sum = initialPoints + actionPoints),100)
        }
    },
    computed: {
        winner: function() {
            return {
                status: this.players.human.health === 0 || this.players.monster.health === 0,
                who: this.players.human.health === 0 ? (this.players.monster.health === 0  ? 'None' : this.players.monster.name) : this.players.human.name,
                score: this.players.human.health === 0 ? this.players.monster.health : this.players.human.health
            }
        }
    },
    watch: {
        winner: function () {
            if(this.winner.status) this.showResults();
        }
    }

})