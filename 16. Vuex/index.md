# Useful Links:
- Vuex Github Page: https://github.com/vuejs/vuex
- Vuex Documenation: https://vuex.vuejs.org/en/
- Auto-namespacing with Vuex 2.1: https://github.com/vuejs/vuex/releases/tag/v2.1.0