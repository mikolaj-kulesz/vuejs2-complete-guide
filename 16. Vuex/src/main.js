import Vue from 'vue'
import App from './App.vue'
import { store } from './store/store.js'

console.log('-->', Vue.version)

new Vue({
    el: '#app',
    store,
    render: h => h(App)
})
