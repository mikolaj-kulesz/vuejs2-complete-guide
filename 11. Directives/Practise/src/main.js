import Vue from 'vue'
import App from './App.vue'

Vue.directive('highlight', {
  bind(el, binding, vnode){
    //el.style.backgroundColor = 'green'
    el.style.backgroundColor = binding.value;
  }
})

Vue.directive('lowlight', {
    bind(el, binding, vnode){
        if(el.style.hasOwnProperty(binding.arg)){
            el.style[binding.arg] = binding.value;
        } else {
            el.innerHTML = 'No such styling!!!'
        }
    }
})

Vue.directive('supercolor', {
    bind(el, binding, vnode){
        if(el.style.hasOwnProperty(binding.arg)){
            el.style[binding.arg] = binding.value.mainColor
        } else {
            el.innerHTML = 'No such styling!!!'
        }
        let myInterval
        if(binding.modifiers['blink']){
          let i = 0;
          myInterval = setInterval(()=>{
              i++;
              if(i%2 === 0) el.style[binding.arg] = binding.value.secColor;
              else el.style[binding.arg] = binding.value.mainColor;
          },binding.value.delay)
        }
        if(binding.modifiers['stop']){
            clearInterval(myInterval);
            el.style[binding.arg] = 'black';
        }
    }
})


Vue.directive('myon', {
    bind(el, binding, vnode){
        el.addEventListener(binding.arg, function() {
            binding.value()
        });
    }
})

new Vue({
  el: '#app',
  render: h => h(App)
})
