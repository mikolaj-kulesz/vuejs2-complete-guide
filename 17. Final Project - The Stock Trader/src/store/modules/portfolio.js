import stocks from '../../data/stocks'

const state = {
    funds: 10000,
    stocks: []
}

const mutations = {
    'BUY_STOCK'(state, {id, qty, price}){
        console.log('BUY_STOCK')
        const record = state.stocks.find(element => element.id === id)
        if (record){
            console.log(typeof record.qty)
            console.log(typeof qty)
            record.qty += qty
        } else {
            state.stocks.push({
                id,
                qty
            })
        }
        state.funds -= price * qty
    },
    'SELL_STOCK'(state, {id, qty, price}){
        const record = state.stocks.find(element => element.id === id)
        if (record.qty > qty){
            record.qty -= qty
        } else {
            state.stocks.splice(state.stocks.indexOf(record), 1)
        }
        state.funds += price * qty
    },
    'SET_PORTFOLIO'(state, portfolio){
        state.funds = portfolio.funds
        state.stocks = portfolio.stockPortfolio ? portfolio.stockPortfolio : []
    },
}

const actions = {
    sellStock: ({ commit }, order) => {
        commit('SELL_STOCK', order)
    }
}

const getters = {
    stockPortfolio: (state, getters) => {
        return state.stocks.map(stock => {
            const record = getters.stocks.find(element => element.id === stock.id)
            return {
                id: stock.id,
                qty: stock.qty,
                name: record.name,
                price: record.price
            }
        })
    },
    funds: state => {
        return state.funds
    }
}

export default {
    state,
    mutations,
    actions,
    getters
}