import Vue from 'vue'

export const loadData = ({ commit}) => {
    Vue
        .http
        .get('data.json')
        .then(response => response.json())
        .then(data => {
            if(data){
                const {funds, stockPortfolio, stocks} = data
                const portfolio = {
                    stockPortfolio,
                    funds
                }

                commit('SET_PORTFOLIO', portfolio)
                commit('SET_STOCKS', stocks)
            }
    })
}