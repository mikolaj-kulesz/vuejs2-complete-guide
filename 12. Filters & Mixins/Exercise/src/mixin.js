export const myMixin = {
    data(){
        return {
            item: 'Lorem ipsum'
        }
    },
    computed: {
        reverse(){
            return this.item.split('').reverse().join('')
        },
        countsLength(){
            return `${this.item} (${this.item.split('').length})`;
        }
    }
}