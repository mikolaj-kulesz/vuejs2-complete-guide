export const myMixin = {
    data(){
        return {
            fruits: ['Apple', 'Banana', 'Mango', 'Melon'],
            filterText: ''
        }
    },
    computed: {
        modifiedFruits(){
            return this.fruits.map(item => `my lowercase ${item.toLowerCase()}`)
        },
        filteredFruits(){
            return this.fruits.filter(item => item.includes(this.filterText))
        }
    },
    created(){
        console.log('created');
    }
}