export default {
  items: [
    {
      id: 23423,
      name: 'Some item',
      color: 'red'
    },
    {
      id: 67545,
      name: 'Some item',
      color: 'black'
    },
    {
      id: 456,
      name: 'Some item',
      color: 'yellow'
    },
    {
      id: 3373423,
      name: 'Some item',
      color: 'green'
    },
  ],
}
