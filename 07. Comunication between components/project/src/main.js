import Vue from 'vue'
import App from './App.vue'
import Data from './data/data'

export const dataBus = new Vue({
  data: function() {
    return Data
  },
  methods: {
    selectedItem(item){
      dataBus.$emit('selectItem', item)
    }
  }
})

new Vue({
  el: '#app',
  render: h => h(App)
})
