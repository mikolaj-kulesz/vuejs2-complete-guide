# Vue Instance ( two version of VueJS)

## Vue with compiler
- Vue builds template base on the "el" or "template" property
- then Vue uses it to re-render it (watches different properties, computed properties, methods...)
- javascript is super fast but accessing the DOM is super slow so Vue is creating Virtual Dom, update the changes there (append the changes firstly to the Virtual DOM), and when this is done, this part is being replaced with the Real DOM ( actually Vue firstly checks all the differs between REAL and VIRTUAL DOM and only this part is being updated) 
- that's how browser DOM is being replaced 

## Vue without the compiler
- we also get another version of Vue without a compiler, so there, the compiler is stripped out,
- we have to compile our templates during the build process so that when we ship our application,
- then we only have compiled javascript code which can get executed whenever we need to re-render the dom or whenever Vue decides to re-render. (this is what it does at runtime anyways)
- precompiled version: it's smaller and faster

## JSFiddle:
- The Vue Instance Code: https://jsfiddle.net/smax/9a2k6cja/2/
- The VueJS Instance Lifecycle: https://jsfiddle.net/smax/jcgw7ak8/

## Useful Links:
- Official Docs - The Vue Instance: http://vuejs.org/guide/instance.html