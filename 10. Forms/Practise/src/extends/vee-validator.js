import {Validator} from 'vee-validate';

class ExtendVeeValidator {
    constructor(){
        this.extendValidators()
    }
    extendValidators(){
        this.addTruthy()
        this.addIsBetween()
    }
    addTruthy(){
        Validator.extend('truthy', {
            getMessage: field => 'The ' + field + ' value is not truthy.',
            validate: value => !! value
        });
    }
    addIsBetween(){
        const isBetween = (value, { min, max } = {}) => {
            return Number(min) <= value && Number(max) >= value;
        };
        // The first param is called 'min', and the second is called 'max'.
        const paramNames = ['min', 'max'];
        Validator.extend('between', isBetween, {
            paramNames //  pass it in the extend options.
        });
    }
}

export default new ExtendVeeValidator();