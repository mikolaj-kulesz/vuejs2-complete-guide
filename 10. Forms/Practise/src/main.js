import Vue from 'vue'
import App from './App.vue'
import VeeValidator from 'vee-validate';
import ExtendVeeValidator from './extends/vee-validator'

Vue.use(VeeValidator, {
    events: 'input|submit'
});

new Vue({
  el: '#app',
  render: h => h(App)
})





